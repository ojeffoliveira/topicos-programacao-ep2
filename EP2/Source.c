#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#define RAND_MAX 500000;

const int limite=0;
//Checa se o array está ordenado de forma crescente
void check(int *Arra, int n) {
	int k;
	for (k = 0; k < n-1; k++) {      // copy the elements from aux[] to a[]
		if (Arra[k] > Arra[k + 1] || Arra[k]<0 || Arra[k+1] <0) {
			printf("\n////////////\n////ERRO////\n////////////\n - Arra[%d] e maior do que Arra[%d]", k, (k + 1));
			return;
		}
	}
	printf("\nALGORITMO FUNCIONA");
}
//Troca dois valores, usado em quick
void swap(int* a, int* b) {
	int temp = *a;
	*a = *b;
	*b = temp;
}
void printArrayP(int *Arra, int start, int end) {
	int k;
	printf("\n");
	for (k = start; k <=end; k++) {      // copy the elements from aux[] to a[]
		printf("%d ", Arra[k]);
	}
	printf("\n\n");

}
void printArray(int *Arra, int n) {
	printArrayP(Arra, 0, n - 1);
}

long long jeffinsertion(int *Array, int start, int end) {
	int i, j, temp;
	long long compara;
	compara = 0;

	for (i = start+1; i <= end; i++)
	{
		temp = Array[i];
		j = i - 1;
		compara++;
		while (temp < Array[j] && j >= start)
		{
			Array[j + 1] = Array[j];
			--j;
			compara++;
		}
		Array[j + 1] = temp;
	}

	return compara;
}

long long jeffsort(int *A, int start, int end, int *aux, int a) {
	long long compara = 0;

	if ((end - start) > a) {
		int mid = (start + end) / 2;
		
		compara = compara + jeffsort(A, start, mid, aux,a);
		//printArrayP(A, start, mid);
		compara = compara + jeffsort(A, (mid+1), end, aux,a);
		//printArrayP(A, mid+1, end);

		int pointer_left = start;       
		int pointer_right = mid + 1;        
		int k;      
		for (k = start; k <= end; k++) {
			if (pointer_left == mid + 1) {      
				aux[k] = A[pointer_right];
				pointer_right++;
			}
			else if (pointer_right == end + 1) {        
				aux[k] = A[pointer_left];
				pointer_left++;
			}
			else if (1) {
				compara++;
				
				if (A[pointer_left] < A[pointer_right]) {      
					aux[k] = A[pointer_left];
					pointer_left++;
				}
				else {       
					aux[k] = A[pointer_right];
					pointer_right++;
				}
			}

		}
		//printArray(a, j - i + 1);
		for (k = start; k <= end; k++) {     
			A[k] = aux[k];
		}
	}

	else {
		compara = compara + jeffinsertion(A, start, end);	
		//printArrayP(A, start, end);
	}
	return compara;

}
long long jeff(int *A, int n, int a, int *aux) {
	return jeffsort(A, 0, n - 1, aux, a);
	
}
//As funcoes de ordenação. 
long long merge_sort(int i, int j, int *a, int *aux) {
	long long comparaa;
	comparaa = 0;
	if (j <= i) {
		return comparaa;     // the subsection is empty or a single element
	}
	int mid = (i + j) / 2;

	// left sub-array is a[i .. mid]
	// right sub-array is a[mid + 1 .. j]

	comparaa = comparaa + merge_sort(i, mid, a, aux);     // sort the left sub-array recursively
	comparaa = comparaa + merge_sort((mid + 1), j, a, aux);     // sort the right sub-array recursively

	int pointer_left = i;       // pointer_left points to the beginning of the left sub-array
	int pointer_right = mid + 1;        // pointer_right points to the beginning of the right sub-array
	int k;      // k is the loop counter

	// we loop from i to j to fill each element of the final merged array
	for (k = i; k <= j; k++) {
		if (pointer_left == mid + 1) {      // left pointer has reached the limit
			aux[k] = a[pointer_right];
			pointer_right++;
		}
		else if (pointer_right == j + 1) {        // right pointer has reached the limit
			aux[k] = a[pointer_left];
			pointer_left++;
		}
		else if (1) {
			comparaa++;
			//printf("\n entrei %d\n", comparaa);
			if (a[pointer_left] < a[pointer_right]) {        // pointer left points to smaller element
				aux[k] = a[pointer_left];
				pointer_left++;
			}
			else {        // pointer right points to smaller element
				aux[k] = a[pointer_right];
				pointer_right++;
			}
		}
		
	}
	//printArray(a, j - i + 1);
	for (k = i; k <= j; k++) {      // copy the elements from aux[] to a[]
		a[k] = aux[k];
	}
	return comparaa;
}

long long  merge(int *A, int n, int *aux) {
	return merge_sort(0, n-1, A, aux);
}


int partition(int *arr, const int low, const int high, int *compara)
{
	int pivot = arr[high];    // pivot 
	int i = (low - 1);  // Index of smaller element 

	for (int j = low; j < high; j++)
	{
		// If current element is smaller than or 
		// equal to pivot 
		(*compara) = (*compara) + 1;
		if (arr[j] <= pivot)
		{
			i++;    // increment index of smaller element 
			swap(&arr[i], &arr[j]);
		}
	}
	swap(&arr[i + 1], &arr[high]);
	return (i + 1);
}
void quickSort(int *arr, int low, int high, int *comparaa)
{
	while (low < high)
	{
		int pi = partition(arr, low, high, comparaa);

		if (pi-low<high-pi) {
			quickSort(arr, low, pi - 1, comparaa);
			low = pi + 1;
		}
		else {
			quickSort(arr, pi + 1, high, comparaa);
			high = pi - 1;
		}

	}

}
void quick(int *Arra, int n, int *comparaa) {
	quickSort(Arra, 0, (n-1), comparaa);
}

long long selectionD(double *Array, int n) {
	int steps, i;
	double temp;
	long long compara;
	compara = 0;
	for (steps = 0; steps < n; ++steps) {
		for (i = steps + 1; i < n; ++i) {
			compara++;
			if (Array[steps] > Array[i]) {
				temp = Array[steps];
				Array[steps] = Array[i];
				Array[i] = temp;
			}
		}
	}
	return compara;

}
long long selection(int *Array, int n) {
	int steps, i, temp;
	long long compara;
	compara = 0;
	for (steps = 0; steps < n; ++steps) {
		for (i = steps + 1; i < n; ++i) {
			compara++;
			if (Array[steps] > Array[i]) {
				temp = Array[steps];
				Array[steps] = Array[i];
				Array[i] = temp;
			}
		}
	}
	return compara;

}

long long insertionD(double *Array, int n) {
	int i, j;
	long long compara;
	compara = 0;
	double temp;
	for (i = 1; i < n; i++)
	{
		temp = Array[i];
		j = i - 1;
		compara++;
		while (temp < Array[j] && j >= 0)
		{
			Array[j + 1] = Array[j];
			--j;
		}
		Array[j + 1] = temp;
	}

	return compara;
}
long long insertion(int *Array, int n) {
	int i, j, temp;
	long long compara;
	compara = 0;

	for (i = 1; i < n; i++)
	{
		temp = Array[i];
		j = i - 1;
		compara++;
		while (temp < Array[j] && j >= 0)
		{
			Array[j + 1] = Array[j];
			--j;
			compara++;
		}
		Array[j + 1] = temp;
	}

	return compara;
}

long long bubble(int *Array, int n) {
	int i, step, temp;
	long long compara;
	compara = 0;

	for (step = 0; step < n - 1; ++step) {
		int swapped = 0;
		for (i = 0; i < n - step - 1; ++i)
		{
			compara++;
			if (Array[i] > Array[i + 1])   /* To sort in descending order, change > to < in this line. */
			{
				swapped = 1;
				temp = Array[i];
				Array[i] = Array[i + 1];
				Array[i + 1] = temp;
			}
		}
		if (swapped == 0)
			break;
	}

	return compara;
}
long long bubbleD(double *Array, int n) {
	int i, step;
	double temp;
	long long compara;
	compara = 0;

	for (step = 0; step < n - 1; ++step) {
		int swapped = 0;
		for (i = 0; i < n - step - 1; ++i)
		{
			compara++;
			if (Array[i] > Array[i + 1])   /* To sort in descending order, change > to < in this line. */
			{
				swapped = 1;
				temp = Array[i];
				Array[i] = Array[i + 1];
				Array[i + 1] = temp;
			}
		}
		if (swapped == 0)
			break;
	}

	return compara;
}

void setArrayCrescD(double *Arra, int n) {
	int i;
	for (i = 0; i < n; i++)
		Arra[i] = (double)i + 1.0;
}
void setArrayDecresD(double *Arra, int n) {
	int i;
	for (i = 0; i < n; i++)
		Arra[i] = (double)n - (double)i;
}
void setArrayRandomD(double *Arra, int n) {
	int i;
	for (i = 0; i < n; i++)
		Arra[i] = (double)(rand() % (n*100));
}

void setArrayCresc(int *Arra, int n) {
	int i;
	for (i = 0; i < n; i++)
		Arra[i] = i+1;
}
void setArrayDecres(int *Arra, int n) {
	int i;
	for (i = 0; i < n; i++)
		Arra[i] = n-i;
}
void setArrayRandom(int *Arra, int n) {
	int i,a;
	a = n * 2;
	for (i = 0; i < n; i++)
		Arra[i] = rand() % a;
}

void RunCrescente( int *arra, int n) {
	long long aux = 0LL;
	clock_t t;
	

	
	printf("\n                                   >>>> ENTRADA CRESCENTE N=%d <<<< \n", n);

	setArrayCresc(arra, n);
	if (n < limite)
		printArray(arra, n);
	printf("\nSelection, N= %d, comparacoes: ", n);
	t = clock();
	aux = selection(arra, n);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000);
	aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	setArrayCresc(arra, n);
	if (n < limite)
		printArray(arra, n);
	printf("\nInsertion, N=%d , comparacoes: ",n);
	t = clock();
	aux = insertion(arra, n);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000); 
	aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	setArrayCresc(arra, n);
	if (n < limite)
		printArray(arra, n);
	printf("\nBubble, N=%d , comparacoes: ", n);
	t = clock();
	aux = bubble(arra, n);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000); 
	aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	setArrayCresc(arra, n);
	printf("\nMerge, N= %d, comparacoes: ", n);
	t = clock();
	int *auxarray;
	auxarray = (int *)malloc(n * sizeof(int));
	aux = merge(arra, n, auxarray);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000); 
	aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	setArrayCresc(arra, n);
	printf("\nQuick, N= %d, comparacoes: ", n);
	t = clock();
	quick(arra, n, &aux);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000);
	aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	
	printf("\JEFF, N= %d, comparacoes: ", n);
	t = clock();
	int *auxi;
	auxi = (int *)malloc(n * sizeof(int));
	aux = jeff(arra, n, 4, auxi);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000); aux = 0;
	aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	free(auxi);
	free(auxarray);
}
void RunDecrescente(int *arra, int n) {
	long long aux = 0LL;

	clock_t t;

	printf("\n                                   >>>> ENTRADA DECRESCENTE N=%d <<<< \n", n);

	setArrayDecres(arra, n);
	if (n < limite)
		printArray(arra, n);
	printf("\nSelection, N= %d, comparacoes: ", n);
	t = clock();
	aux = selection(arra, n);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000); 
	aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	setArrayDecres(arra, n);
	if (n < limite)
		printArray(arra, n);
	printf("\nInsertion, N=%d , comparacoes: ", n);
	t = clock();
	aux = insertion(arra, n);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000); 
	aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	setArrayDecres(arra, n);
	if (n < limite)
		printArray(arra, n);
	printf("\nBubble, N=%d , comparacoes: ", n);
	t = clock();
	aux = bubble(arra, n);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000); 
	aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	setArrayDecres(arra, n);
	printf("\nMerge, N= %d, comparacoes: ", n);
	t = clock();
	int *auxarray;
	auxarray = (int *)malloc(n * sizeof(int));
	aux = merge(arra, n, auxarray);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000); aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	setArrayDecres(arra, n);
	printf("\nQuick, N= %d, comparacoes: ", n);
	t = clock();
	quick(arra, n, &aux);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000);aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	
	printf("\JEFF, N= %d, comparacoes: ", n);
	t = clock();
	int *auxi;
	auxi = (int *)malloc(n * sizeof(int));
	aux = jeff(arra, n, 4, auxi);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000); aux = 0;
	aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	free(auxi);

	free(auxarray);
}
//Passa os valores de um array para outro, para manter o 
//array igual quando rodando randomicamente
void translate(int *a, int *b, int n) {
	int k;
	for (k = 0; k < n; k++) {      // copy the elements from aux[] to a[]
		a[k] = b[k];
	}
}

void teste() {
	int n;
	scanf_s("%d", &n);
	int i = n / 2;
	int level = 1;
	int merge = (n / 2)*(log(n) / log(2));
	int insert = ((n*n + n - 2) / 2);
	printf("Com N=%d Merge=%d e Insertion=%d \n\n", n,merge,insert);

	while (i >= 2) {
		int resul = level * (n / 2);
		int minim = resul + pow(2,level) * i;
		resul = resul + (((i*i) + i - 2) / 2)*pow(2, level);
		double meu = 2 * (((log(i) / log(2))) - 1);
		meu = pow(2, meu);
		meu = level * (n / 2) + meu * pow(2, level);
		printf("Com N =%d level=%d e i=%d temos max %d  min %d jeffsort: %lf \n", n, level, i,resul, minim, meu);
		i = i / 2;
		level++;
	}
}

void counter(int *array, int n, char output[]) {
		FILE *fp;
		
		
	
		int j;
			
		fopen_s(&fp,output,"w");
	
		for (j = 0; j < n; j++) {
			//printf(" %d\n", (array[j]));
				fprintf(fp, "%d\n", (array[j]));
		}
		fprintf(fp, "\n\n\n");
		fclose(fp);
}

void distribui(int *valoresz, int *valoresu,int *resul, int n) {
	int v, r;
	for (v = 0, r=0; v < n;) {
		if (valoresz[v] == resul[r]) {
			valoresu[v]++;
			r++;
		}
		else
			v++;
	}

}
void printDistrit (int *valoresz, int *valoresu, int n){
	int v,j;
	for (v = 0; v < n;v++) {
		printf("\n");
		if (valoresz[v] < 1000000)
			printf(" ");
		if (valoresz[v] < 100000)
			printf(" ");
		if (valoresz[v] < 10000)
			printf(" ");
		if (valoresz[v] < 1000)
			printf(" ");
		if (valoresz[v] < 100)
			printf(" ");

		printf("%d :", valoresz[v]);
		
		for(j=0;j< valoresu[v]; j++)
			printf(".");
	}

}
void insertionteste() {
	int numeTestes = 8000000;
	int a;
	int *resul;
	resul = (int *)malloc(numeTestes * sizeof(int));
	
	for (a = 0; a < numeTestes; a++) {
		if(a%(numeTestes/50)== (numeTestes / 20)-1)
			printf(".");
		int n;
		n = 128;
		int *aux;
		aux = (int *)malloc(n * sizeof(int));
		int *arra;
		arra = (int *)malloc(n * sizeof(int));
		setArrayRandom(arra, n);
		resul[a] = jeff(arra, n, 3,aux);
		free(arra);
		free(aux);
	}

	
	{
		int *arra;
		arra = (int *)malloc(numeTestes * sizeof(int));
		merge(resul, numeTestes, arra);
	}
	
	int *valoresz, *valoresu, i, j;
	int range = (resul[numeTestes - 1] - resul[0]) + 1;
	valoresz = (int *)malloc(range * sizeof(int));
	valoresu = (int *)malloc(range * sizeof(int));
	for (i = resul[0], j = 0; i <= resul[numeTestes - 1]; i++, j++) {
		valoresz[j] = i;
		valoresu[j] = 0;
	}

	//printArray(valoresz, range);
	distribui(valoresz, valoresu, resul, range);
	//printArray(valoresu, range);
	//printDistrit(valoresz,valoresu, range);
	counter(valoresz, range, "output.txt");
	counter(valoresu, range, "outputV.txt");
	printf("\n\n >>>>>>>>><<<<<<<<<<<\n\n");

	free(resul);
	free(valoresu);
	free(valoresz);
}

void RunRandom(int *arra, int n) {
	long long aux = 0LL;
	clock_t t;

	printf("\n                                   >>>> ENTRADA RANDOM N=%d <<<< \n", n);

	setArrayRandom(arra, n);
	int *b;
	b = (int *)malloc(n * sizeof(int));
	translate(b, arra, n);
	
	if (n < limite)
		printArray(arra, n);
	printf("\nSelection, N= %d, comparacoes: ", n);
	t = clock();
	aux = selection(arra, n);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000); aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	translate(arra, b, n);
	printf("\nInsertion, N=%d , comparacoes: ", n);
	t = clock();
	aux = insertion(arra, n);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000); aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	translate(arra, b, n);
	printf("\nBubble, N=%d , comparacoes: ", n);
	t = clock();
	aux = bubble(arra, n);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000); aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	translate(arra, b, n);
	printf("\nMerge, N= %d, comparacoes: ", n);
	t = clock();
	int *auxarray;
	auxarray = (int *)malloc(n * sizeof(int));
	aux = merge(arra, n, auxarray);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000); aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	translate(arra, b, n);
	printf("\nQuick, N= %d, comparacoes: ", n);
	t = clock();
	quick(arra, n, &aux);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000);aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);
	

	printf("\JEFF, N= %d, comparacoes: ", n);
	t = clock();
	int *auxi;
	auxi = (int *)malloc(n * sizeof(int));
	aux = jeff(arra, n, 4, auxi);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000); aux = 0;
	aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	free(auxi);
	free(auxarray);
}

void TesteJeff(int *arra, int n, int a) {
	clock_t t;
	long long aux = 0LL;
	setArrayRandom(arra, n);
	int *b;
	b = (int *)malloc(n * sizeof(int));
	translate(b, arra, n);
	
	printf("\n                                   >>>> TESTE JEFFSORT N=%d <<<< \n", n);
	if (n < limite)
		printArray(arra, n);
	printf("\JEFF, N= %d, comparacoes: ", n);
	t = clock();
	int *auxi;
	auxi = (int *)malloc(n * sizeof(int));
	aux = jeff(arra, n, a, auxi);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000); aux = 0;
	aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	translate(arra, b, n);
	printf("\nInsertion, N=%d , comparacoes: ", n);
	t = clock();
	aux = insertion(arra, n);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000); aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);

	translate(arra, b, n);
	printf("\nMerge, N= %d, comparacoes: ", n);
	t = clock();
	int *auxarray;
	auxarray = (int *)malloc(n * sizeof(int));
	aux = merge(arra, n, auxarray);
	t = clock() - t;
	printf("%d, tempo: %d.%d segundos", aux, t / 1000000, t % 1000); aux = 0;
	check(arra, n);
	if (n < limite)
		printArray(arra, n);
	free(auxarray);
	free(auxi);
}
int main()
{
	srand(time(NULL));   // Initialization, should only be called once.
	

						 //insertionteste();

	while (1) {
		teste();
	}
	//int n,aa;
	//n = 16;
	//aa = 2;
	//while(1)
	//{
	//	int *arra;
	//	arra = (int *)malloc(n * sizeof(int));
	//	TesteJeff(arra, n,5);
	//	scanf_s("");
	//	//n = n + aa;
	//	//aa = aa+2;
	//}


	{
		int n;
		n = 10;
		int arra[10];
		RunCrescente(arra, n);
		RunDecrescente(arra, n);
		RunRandom(arra, n);
	}
	
	{
		int n;
		n = 200;
		int *arra;
		arra = (int *)malloc(n * sizeof(int));
		RunCrescente(arra, n);
		RunDecrescente(arra, n);
		RunRandom(arra, n);
		free(arra);
	}
	{
		int n;
		n = 50;
		int arra[50];
		RunCrescente(arra, n);
		RunDecrescente(arra, n);
		RunRandom(arra, n);
	}
	{
		int n;
		n = 2500;
		int arra[2500];
		RunCrescente(arra, n);
		RunDecrescente(arra, n);
		RunRandom(arra, n);
	}
	{
		int n;
		n = 30000;
		int *arra;
		arra = (int *)malloc(n * sizeof(int));
		RunCrescente(arra, n);
		RunDecrescente(arra, n);
		RunRandom(arra, n);
		free(arra);
	}
	
	int a;
	scanf_s("%d", &a);
system("pause");
return 0;

}
