==================

Este projeto foi o segundo exercício programa desenvolvido 
para o curso de verão Tópicos de Programação 2019 no IME USP. 
Professora responsável Patrícia Alves.
==================

    O projeto consistiu na implementação de algoritmos para ordenar listas de  números 
incluindo a criação de um algoritmo próprio que tentasse juntar características 
dos algoritmos clássicos.
    Com a implementação feita o programa deveria rodar testes de complexidade
em cada um dos algoritmos usando como unidade de complexidade o tempo e
sobretudo o número de comparações entre números das listas.